package demo.android.moneytap.com.moneytap.framework.model;

import java.io.Serializable;

/**
 * Created by mujeeb on 26/01/17.
 */
/**
 * Wrapper object to identify different reference.model's moving between layers
 */
public interface Model extends Serializable {

}
